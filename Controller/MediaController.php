<?php

	namespace KMS\FroalaEditorBundle\Controller;

	use KMS\FroalaEditorBundle\Service\MediaManager;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\KernelInterface;

	/**
	 * Media controller.
	 * Class MediaController
	 * @package KMS\FroalaEditorBundle\Controller
	 */
	class MediaController extends Controller
	{

		// -------------------------------------------------------------//
		// --------------------------- METHODS -------------------------//
		// -------------------------------------------------------------//

		/**
		 * Upload an image.
		 * @param \Symfony\Component\HttpFoundation\Request $p_request
		 * @return \Symfony\Component\HttpFoundation\JsonResponse
		 */
		public function uploadImageAction(Request $p_request, MediaManager $mediaManager, KernelInterface $kernel)
		{
			$path         = $p_request->request->get( "path" );
			$folder       = $p_request->request->get( "folder" );
			$rootDir      = $kernel->getProjectDir();
			$basePath     = $p_request->getBasePath();
			// ------------------------- DECLARE ---------------------------//

			// FIXME
//			if( $request->isXmlHttpRequest() == true )
//			{
				return $mediaManager->uploadImage( $p_request->files, $rootDir, $basePath, $folder, $path );
//			}
		}

		/**
		 * Delete an image.
		 * @param \Symfony\Component\HttpFoundation\Request $p_request
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function deleteImageAction(Request $p_request, MediaManager $mediaManager, KernelInterface $kernel)
		{
			$imageSrc     = $p_request->request->get( "src" );
			$folder       = $p_request->request->get( "folder" );
			$rootDir      = $kernel->getProjectDir();
			// ------------------------- DECLARE ---------------------------//

			$mediaManager->deleteImage( $imageSrc, $rootDir, $folder );

			return new Response ();
		}

		/**
		 * Load images.
		 * @param \Symfony\Component\HttpFoundation\Request $p_request
		 * @return \Symfony\Component\HttpFoundation\JsonResponse
		 */
		public function loadImagesAction(Request $p_request, MediaManager $mediaManager, KernelInterface $kernel)
		{
			$path         = $p_request->query->get( "path" );
			$folder       = $p_request->query->get( "folder" );
			$rootDir      = $kernel->getProjectDir();
			$basePath     = $p_request->getBasePath();

			// ------------------------- DECLARE ---------------------------//

			return $mediaManager->loadImages( $rootDir, $basePath, $folder, $path );
		}

		/**
		 * Upload a file.
		 * @param \Symfony\Component\HttpFoundation\Request $p_request
		 * @return \Symfony\Component\HttpFoundation\JsonResponse
		 */
		public function uploadFileAction(Request $p_request, MediaManager $mediaManager, KernelInterface $kernel)
		{
			$path         = $p_request->request->get( "path" );
			$folder       = $p_request->request->get( "folder" );
			$rootDir      = $kernel->getProjectDir();
			$basePath     = $p_request->getBasePath();
			// ------------------------- DECLARE ---------------------------//

			// FIXME
//			if( $request->isXmlHttpRequest() == true )
//			{
			return $mediaManager->uploadFile( $p_request->files, $rootDir, $basePath, $folder, $path );
//			}
		}

		/**
		 * Upload a video.
		 * @param \Symfony\Component\HttpFoundation\Request $p_request
		 * @return \Symfony\Component\HttpFoundation\JsonResponse
		 */
		public function uploadVideoAction(Request $p_request, MediaManager $mediaManager, KernelInterface $kernel)
		{
			$path         = $p_request->request->get( "path" );
			$folder       = $p_request->request->get( "folder" );
			$rootDir      = $kernel->getProjectDir();
			$basePath     = $p_request->getBasePath();
			// ------------------------- DECLARE ---------------------------//

			// FIXME
//			if( $request->isXmlHttpRequest() == true )
//			{
			return $mediaManager->uploadVideo( $p_request->files, $rootDir, $basePath, $folder, $path );
//			}
		}

	}
